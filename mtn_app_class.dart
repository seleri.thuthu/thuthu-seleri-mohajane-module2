class MtnApp {                                  //Create class of MTN App of the Year winner
  String appName = '';
  String sectorCategoryName = '';
  String developerName = '';
  var winYear = '';
  String toCapital() {
    return appName.toUpperCase();
  }
  @override
  String toString() {
    return 'App name: $appName, Sector/Category: $sectorCategoryName, Developer: $developerName, Year won: $winYear';
  }
}

void main() {
  MtnApp mtnWinner = MtnApp();
  mtnWinner.appName = 'fnb';
  mtnWinner.sectorCategoryName = 'Banking - Best iOS Consumer App';
  mtnWinner.developerName = 'FNB';
  mtnWinner.winYear = '2012';
  print(mtnWinner);
  print("Winning App capitalised is: " + mtnWinner.toCapital());
}

