//List of winners since 2012
final pastWinners = ['FNB', 'SnapScan', 'LIVE Inspect', 'WumDrop', 'Domestly', 'Shyft', 'Khula ecosystem', 'Naked', 'EasyEquities', 'Ambani Africa'];
void main() {
    print('Past MTN App of the Year winners since 2012 to 2021 are:');
  print(' ');
  var count = 0 ;
  while(count < pastWinners.length){                      //Print overall winners since 2012
    print(pastWinners[count]);
    count++;
  }
  pastWinners.sort();                                     // Sort overall winners by name
  print(' ');
  var countSorted = 0;
  print('Past winners sorted by name are:');
  print(' ');
  while(countSorted < pastWinners.length){
    print(pastWinners[countSorted]);                      // Print sorted list of winners
    countSorted++;
  }
  print(' ');
  print('Winning App for 2017 is:');
  print(pastWinners[7]);                                  // The winner for 2017
  print(' ');
  print('Winning App for 2018 is: ');
  print(pastWinners[4]);                                  // The winner for 2018
  print(' ');
  print('Total number of Apps is:');
  print(pastWinners.length);                              // Print the total number of overall winners since 2012
}
